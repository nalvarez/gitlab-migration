#!/usr/bin/python3

# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: MIT

"""
Moves projects from the kde/ namespace to the new structure according to repo-metadata
"""

# Groups that will be entirely skipped despite being in repo-metadata
SKIP_GROUPS = ['websites', 'sysadmin', 'documentation', 'others', 'wikitolearn']

import os
import sys
import argparse
import yaml
import gitlab

path = os.path

parser = argparse.ArgumentParser(
        description="Move repositories to their proper KDE GitLab structure")
parser.add_argument("--repo-metadata", dest='metadata_path', metavar='PATH', required=True, help="Path to repo-metadata checkout")
parser.add_argument("--real-run", action='store_true', help="Use GitLab API for real (dry-run is the default)")
parser.add_argument("--create-groups", action='store_true', help="Create groups")
parser.add_argument("--move-repos", action='store_true', help="Transfer repositories from kde/ to final structure. Assumes groups exist (or that --create-groups was passed)")
parser.add_argument("--group", action='append', help="Process only this group (can be used multiple times)")
parser.add_argument("--skip", action='append', help="Do *not* process this group (can be used multiple times)")
parser.add_argument("--test-mode", action='store_true', help="Use test group and repos") # Needs manual creation of stuff in GitLab first!

# copied off gitlab cli
parser.add_argument(
    "-c",
    "--config-file",
    action="append",
    help="python-gitlab configuration file to use.",
)
parser.add_argument(
    "--gitlab",
    help=(
        "Which gitlab configuration section should "
        "be used. If not defined, the default selection "
        "will be used."
    ),
    required=False
)

args = parser.parse_args()
print(args)

if not any([args.create_groups, args.move_repos]):
    print("Nothing to do")
    sys.exit(0)

if args.real_run:
    gl = gitlab.Gitlab.from_config(args.gitlab, args.config_file)
    print(f"Will connect to GitLab instance at {gl.url}")
    actions=[]
    if args.create_groups: actions.append("create groups")
    if args.move_repos: actions.append("move repositories into groups")
    print()
    prompt="We're about to actually make changes in GitLab, will "+' and '.join(actions)+",\nare you sure you want to continue? (type 'yes') "
    if input(prompt) != 'yes':
        print("Aborting")
        sys.exit(1)

base_path = path.join(args.metadata_path, 'projects-invent/')
# namespace where the repos currently are, and will be moved from
prefix = 'kde'
if args.test_mode:
    prefix = 'testsrc'

def get_namespaces():
    for subdir in os.listdir(base_path):
        if not path.isdir(path.join(base_path, subdir)): continue

        f = None
        try:
            f = open(path.join(base_path, subdir, 'metadata.yaml'), 'r')
            metadata_yaml = yaml.safe_load(f)
            yield (subdir, metadata_yaml)
        except FileNotFoundError:
            yield (subdir, None)
        finally:
            if f: f.close()

def get_projects(namespace):
    namespace_path = path.join(base_path, namespace)
    for subdir in os.listdir(namespace_path):
        if not path.isdir(path.join(namespace_path, subdir)): continue

        if not path.isfile(path.join(namespace_path, subdir, 'metadata.yaml')):
            print("How the hell does '{}' not have metadata?".format(namespace_path + '/' + subdir))

        yield subdir

if args.test_mode:
    def get_namespaces():
        yield "group-one", None
        yield "group-two", {'name': "Second group", 'description': "This is the second test group"}

    def get_projects(namespace):
        if namespace == "group-one":
            yield "one-a"
            yield "one-b"
        elif namespace == "group-two":
            yield "two-c"

namespaces = list(get_namespaces())

if args.create_groups:
    print("Creating groups")
    for ns_name, ns_metadata in namespaces:
        if args.group and ns_name not in args.group:
            continue
        if args.skip and ns_name in args.skip:
            print(f"Skipping {ns_name} as requested")
            continue
        if ns_name in SKIP_GROUPS:
            print(f"Skipping special group '{ns_name}'")
            continue

        group_info = {'path': ns_name}
        if ns_metadata:
            if 'name' in ns_metadata:
                group_info['name'] = ns_metadata['name']

        if 'name' not in group_info:
            # If metadata.yaml doesn't define a human-friendly name, use the path
            # as name; the 'name' key is required by GitLab
            group_info['name'] = ns_name

        if args.real_run:
            print(f"Creating group with info {group_info}")
            gl.groups.create(group_info)
        else:
            print(f"Would call gl.groups.create({group_info})")

if args.move_repos:
    print("Moving projects")
    for ns_name, ns_metadata in namespaces:
        if args.group and ns_name not in args.group:
            continue
        if args.skip and ns_name in args.skip:
            print(f"Skipping {ns_name} as requested")
            continue
        if ns_name in SKIP_GROUPS:
            print(f"Skipping special group '{ns_name}'")
            continue

        for project_name in get_projects(ns_name):
            #print("Would call gitlab.projects.get('kde/{}')".format(project_name))

            if args.real_run:
                print(f"Getting project {prefix}/{project_name}")
                gl_project = gl.projects.get(f'{prefix}/{project_name}')
                if gl_project:
                    print(f"Moving project {project_name} to namespace {ns_name}")
                    gl_project.transfer_project(ns_name)
                else:
                    print(f"Couldn't find repository '{prefix}/{project_name}'; skipping!")
            else:
                # dry run
                print(f"Would call gl.projects.get('{prefix}/{project_name}').transfer_project('{ns_name}')")
